//
// Created by mikaj on 15.03.2023.
//
#include <stdio.h>

int main(){

    float a = 0;
    float b = 0;

    printf("Taschenrechner Addierer\n");
    printf("Gib die 1. Zahl ein\n");
    scanf("%f", &a);
    printf("Gib die 2. Zahl ein\n");
    scanf("%f", &b);

   printf("Das Ergebnis ist: %f", a+b);

    return 0;
}
